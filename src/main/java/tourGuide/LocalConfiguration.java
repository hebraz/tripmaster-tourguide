package tourGuide;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

import java.util.Locale;

@Configuration
public class LocalConfiguration {
    @Bean
    LocaleResolver localeResolver(){
        FixedLocaleResolver l = new FixedLocaleResolver(Locale.US);
        Locale.setDefault(Locale.US);
        return l;
    }
}
