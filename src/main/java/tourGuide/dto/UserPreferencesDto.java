package tourGuide.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Currency;

import javax.money.CurrencyUnit;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@Setter
/**
 * User preference DTO
 */
public class UserPreferencesDto {
    private int attractionProximity;
    @Pattern(regexp = "AUD|CAD|CHF|EUR|GBP|JPY|USD")
    private String currencyUnit;
    @Min(0)
    private BigDecimal lowerPricePointInCents;
    @Min(0)
    private BigDecimal highPricePointInCents;
    @Min(1)
    private int tripDuration;
    @Min(0)
    private int ticketQuantity;
    @Min(1)
    private int numberOfAdults;
    @Min(0)
    private int numberOfChildren;

    public UserPreferencesDto(int attractionProximity, String currencyUnit, BigDecimal lowerPricePointInCents, BigDecimal highPricePointInCents, int tripDuration, int ticketQuantity, int numberOfAdults, int numberOfChildren) {
        this.attractionProximity = attractionProximity;
        this.currencyUnit = currencyUnit;
        this.lowerPricePointInCents = lowerPricePointInCents;
        this.highPricePointInCents = highPricePointInCents;
        this.tripDuration = tripDuration;
        this.ticketQuantity = ticketQuantity;
        this.numberOfAdults = numberOfAdults;
        this.numberOfChildren = numberOfChildren;
    }
    public UserPreferencesDto(){}
}
