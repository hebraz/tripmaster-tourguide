package tourGuide.dto;

import gpsUtil.location.Location;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttractionDto {
    String name;
    Location location;
    double distance;
    int rewardPoints;

    public AttractionDto(String name, Location location, double distance, int rewardPoints) {
        this.name = name;
        this.location = location;
        this.distance = distance;
        this.rewardPoints = rewardPoints;
    }
}
