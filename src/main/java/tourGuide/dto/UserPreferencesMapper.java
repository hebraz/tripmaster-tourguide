package tourGuide.dto;

import tourGuide.user.UserPreferences;

/**
 * Mapper between a UserPreferencesDto an o UserPreferences entity
 */
public interface UserPreferencesMapper {
    /**
     * Maps a UserPreferencesDto to a UserPreferences entity
     * @param userPreferencesDto Dto instance
     * @return entity instance
     */
    UserPreferences mapDtoToEntity(UserPreferencesDto userPreferencesDto);
}
