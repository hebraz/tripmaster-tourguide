package tourGuide.dto;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;
import tourGuide.user.UserPreferences;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.math.BigDecimal;

@Component
/**
 * Implements a UserPreferencesMapper
 */
public class UserPreferencesMapperImpl implements UserPreferencesMapper {

    @Override
    /**
     * Maps a UserPreferencesDto to a UserPreferences entity
     * @param userPreferencesDto Dto instance
     * @return entity instance
     */
    public UserPreferences mapDtoToEntity(UserPreferencesDto userPreferencesDto) {

        CurrencyUnit currencyUnit =  Monetary.getCurrency(userPreferencesDto.getCurrencyUnit());

        return new UserPreferences(
                userPreferencesDto.getAttractionProximity(),
                currencyUnit,
                Money.of(userPreferencesDto.getLowerPricePointInCents().divide(BigDecimal.valueOf(100)),currencyUnit),
                Money.of(userPreferencesDto.getHighPricePointInCents().divide(BigDecimal.valueOf(100)), currencyUnit),
                userPreferencesDto.getTripDuration(),
                userPreferencesDto.getTicketQuantity(),
                userPreferencesDto.getNumberOfAdults(),
                userPreferencesDto.getNumberOfChildren());
    }
}
