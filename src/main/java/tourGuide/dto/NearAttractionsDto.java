package tourGuide.dto;

import gpsUtil.location.Location;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NearAttractionsDto {
    Location userLocation;
    List<AttractionDto> attractions;

    public NearAttractionsDto(Location userLocation, List<AttractionDto> attractions) {
        this.userLocation = userLocation;
        this.attractions = attractions;
    }

    public NearAttractionsDto() {
    }
}
