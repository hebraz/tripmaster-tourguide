package tourGuide.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.w3c.dom.Attr;
import rewardCentral.RewardCentral;
import tourGuide.Utils;
import tourGuide.user.User;
import tourGuide.user.UserReward;

@Service
public class RewardsService {


	// proximity in miles
    private int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	private final GpsUtil gpsUtil;
	private final RewardCentral rewardsCentral;
	private final ExecutorService executorService;

	public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
		executorService =  Executors.newFixedThreadPool(100);
		addShutDownHook();
	}
	
	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}
	
	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}
	
	public Future calculateRewards(User user) {

		Future f = executorService.submit(() -> {
			CopyOnWriteArrayList<VisitedLocation> threadSafeUserLocations = new CopyOnWriteArrayList(user.getVisitedLocations());
			List<Attraction>  attractions = gpsUtil.getAttractions();
			for(VisitedLocation visitedLocation : threadSafeUserLocations) {
				for (Attraction attraction : attractions) {
					if (nearAttraction(visitedLocation, attraction)) {
						synchronized (user){
							if (user.getUserRewards().stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {
								user.addUserReward(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
							}
						}
					}
				}
			}
			return null;
		});
		return f;
	}
	
	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return Utils.getDistance(attraction, location) > attractionProximityRange ? false : true;
	}
	
	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return Utils.getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}
	
	public int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				executorService.shutdown();
			}
		});
	}
}
