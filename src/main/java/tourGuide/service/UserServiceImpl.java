package tourGuide.service;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tourGuide.dto.UserPreferencesDto;
import tourGuide.dto.UserPreferencesMapper;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Implements UserService interface
 *
 */
@Service
public class UserServiceImpl implements UserService {

    private UserPreferencesMapper userPreferencesMapper;

    @Autowired
    public UserServiceImpl(UserPreferencesMapper userPreferencesMapper) {
        this.userPreferencesMapper = userPreferencesMapper;
    }

    /**
     * Updates user preferences
     * @param user user to which preferences are updated
     * @param userPreferencesDto new preferences
     */
    @Override
    public void updateUserPreferences(User user, UserPreferencesDto userPreferencesDto){
        UserPreferences userPreferences = userPreferencesMapper.mapDtoToEntity(userPreferencesDto);
        user.setUserPreferences(userPreferences);
    }
}
