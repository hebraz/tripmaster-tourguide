package tourGuide.service;

import gpsUtil.location.Location;
import tourGuide.dto.UserPreferencesDto;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import java.util.Map;

/**
 * Represents a user service
 */
public interface UserService {
    /**
     * Updates user preferences
     * @param user user to which preferences are updated
     * @param userPreferencesDto new preferences
     */
    void updateUserPreferences(User user, UserPreferencesDto userPreferencesDto);

}
