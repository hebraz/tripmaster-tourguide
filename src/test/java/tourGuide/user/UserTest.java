package tourGuide.user;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static org.junit.Assert.*;

public class UserTest {

    private    User user;
    private VisitedLocation visitedLocation1;
    private VisitedLocation visitedLocation2;
    private VisitedLocation visitedLocation3;

    @Before
    public  void initTest() {
        user = new User(UUID.randomUUID(), "pierrePaul", "0606060606", "pierre.paul.oc@gmaiil.com");
        visitedLocation1 = new VisitedLocation(user.getUserId(), new Location(33.33, -44.44), Date.from(Instant.now().minus(1, ChronoUnit.SECONDS)));
        visitedLocation2 = new VisitedLocation(user.getUserId(), new Location(1.15, -98.36), Date.from(Instant.now()));
        visitedLocation3 = new VisitedLocation(user.getUserId(), new Location(1234.4, -989.54), Date.from(Instant.now().minus(4, ChronoUnit.SECONDS)));
    }

    @Test
    public void addUserReward() {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        user.addUserReward(new UserReward(null,new Attraction("Futuroscope","Poitiers","France",12.012,215.565)));
        user.addUserReward(new UserReward(null,new Attraction("Puy Du Fou","Epesses","France",12.012,215.565)));
        assertEquals(2, user.getUserRewards().size());
        assertTrue(user.getUserRewards().get(0).attraction.attractionName.equals("Futuroscope"));
        assertTrue(user.getUserRewards().get(1).attraction.attractionName.equals("Puy Du Fou"));
    }

    @Test
    public void addUserRewardTwice() {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        user.addUserReward(new UserReward(null,new Attraction("Futuroscope","Poitiers","France",12.012,215.565)));
        user.addUserReward(new UserReward(null,new Attraction("Puy Du Fou","Epesses","France",12.012,215.565)));
        user.addUserReward(new UserReward(null,new Attraction("Futuroscope","Poitiers","France",12.012,215.565)));

        assertEquals(2, user.getUserRewards().size());
        assertTrue(user.getUserRewards().get(0).attraction.attractionName.equals("Futuroscope"));
        assertTrue(user.getUserRewards().get(1).attraction.attractionName.equals("Puy Du Fou"));
    }

    @Test
    public void getLastLocationNoLocation(){

        VisitedLocation lastLoc = user.getLastVisitedLocation();

        assertTrue(Objects.isNull(lastLoc));
    }

    @Test
    public void getLastLocationOneLocation(){
        user.addToVisitedLocations(visitedLocation1);
        VisitedLocation lastLoc = user.getLastVisitedLocation();
        assertFalse(Objects.isNull(lastLoc));
        assertEquals(lastLoc.location.latitude, visitedLocation1.location.latitude, 0.001);
        assertEquals(lastLoc.location.longitude, visitedLocation1.location.longitude, 0.001);
    }



    @Test
    public void getLastLocationSeveralLocation(){
        user.addToVisitedLocations(visitedLocation1);
        user.addToVisitedLocations(visitedLocation2);
        user.addToVisitedLocations(visitedLocation3);

        /*Although the visitedLocation3 hass been added last, visitedLocation2 is the more recent
        Shall return visitedLocation2
         */

        VisitedLocation lastLoc = user.getLastVisitedLocation();
        assertFalse(Objects.isNull(lastLoc));
        assertEquals(lastLoc.location.latitude, visitedLocation2.location.latitude, 0.001);
        assertEquals(lastLoc.location.longitude, visitedLocation2.location.longitude, 0.001);
    }
}