package tourGuide;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import tourGuide.dto.UserPreferencesDto;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.user.User;
import java.math.BigDecimal;
import java.util.UUID;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TourGuideControllerTest {

    @Mock
    UserService userService;
    @Mock
    TourGuideService tourGuideService;

    private TourGuideController tourGuideController;
    @Before
    public void initTest(){
        tourGuideController = new TourGuideController(tourGuideService, userService);
    }
    @Test
    public void setUserPreferences() {
        User user = new User(UUID.randomUUID(),"v","0606060606","pierre.paul.oc@gmaiil.com");
        UserPreferencesDto  userPreferencesDto = new UserPreferencesDto(5000,"EUR", BigDecimal.valueOf(100030),BigDecimal.valueOf(543287),7,5,4,3);

        when(tourGuideService.getUser("pierrePaul")).thenReturn(user);

        tourGuideController.setUserPreferences("pierrePaul",userPreferencesDto);

        verify(userService, times(1)).updateUserPreferences(user,userPreferencesDto);

    }
}