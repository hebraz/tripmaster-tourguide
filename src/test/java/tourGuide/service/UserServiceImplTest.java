package tourGuide.service;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import tourGuide.dto.UserPreferencesDto;
import tourGuide.dto.UserPreferencesMapper;
import tourGuide.dto.UserPreferencesMapperImpl;
import tourGuide.user.User;

import javax.money.NumberValue;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class UserServiceImplTest {

    private   UserPreferencesMapper userPreferencesMapper;
    private   UserService userService;
    private    User user;

    @Before
    public  void initTest(){
        userPreferencesMapper = new UserPreferencesMapperImpl();
        userService = new UserServiceImpl(userPreferencesMapper);
        user = new User(UUID.randomUUID(), "pierrePaul", "0606060606", "pierre.paul.oc@gmaiil.com");
    }

    @Test
    public void updateUserPreferences() {
        UserPreferencesDto  userPreferencesDto = new UserPreferencesDto(5000,"EUR", BigDecimal.valueOf(100030),BigDecimal.valueOf(543287),7,5,4,3);

        userService.updateUserPreferences(user,userPreferencesDto );

        assertEquals(5000,user.getUserPreferences().getAttractionProximity());
        assertEquals("EUR",user.getUserPreferences().getCurrency().getCurrencyCode());
        assertTrue(user.getUserPreferences().getLowerPricePoint().getNumber().numberValue(BigDecimal.class).equals(BigDecimal.valueOf(1000.30)));
        assertTrue(user.getUserPreferences().getHighPricePoint().getNumber().numberValue(BigDecimal.class).equals(BigDecimal.valueOf(5432.87)));
        assertEquals(7,user.getUserPreferences().getTripDuration());
        assertEquals(5,user.getUserPreferences().getTicketQuantity());
        assertEquals(4,user.getUserPreferences().getNumberOfAdults());
        assertEquals(3,user.getUserPreferences().getNumberOfChildren());
    }
}